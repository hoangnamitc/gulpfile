// KHAI BAO THU VIEN
const
    gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    browserSync  = require('browser-sync').create(),
    plumber      = require('gulp-plumber'),
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer');

// KHAI BAO SASS
var _sass = {
    src: [
            "node_modules/bootstrap-sass/assets/stylesheets/bootstrap.scss",
            "node_modules/font-awesome/scss/font-awesome.scss",
            "dev/scss/*.scss"
    ],
    dest: "dev/html/css",
    opt: {
        outputStyle: 'compressed',
        errLogToConsole: true,
        includePaths: []
    }
}

// KHAI BAO JS
var _js = {
    src: [
        "node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js",
        "dev/js/*.js"
    ],
    dest: "dev/html/js"
}

// KHAI BAO FONT
var _font = {
    src: [
        "node_modules/font-awesome/fonts/*"
    ],
    dest: "dev/html/fonts"
}

// KHAI BAO HTML
var _html = {
    src: [
        "dev/html/*.html"
    ],
    dest: "./dev/html"
}

// KHAI BAO SERVER
var _serverName = "serve",
_serverMove = [
    'js',
    'fonts',
    _serverName
];


// Compile SASS
gulp.task('sass', function() {
    return gulp.src(_sass.src)
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sourcemaps.init())
        .pipe(sass(_sass.opt).on('error', sass.logError))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(autoprefixer({ browsers: ['last 2 version', '> 5%'] }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(_sass.dest))
        .pipe(browserSync.stream());
});

// Move JS
gulp.task('js', function() {
    return gulp.src(_js.src)
        .pipe(gulp.dest(_js.dest))
        .pipe(browserSync.stream());
});

// Move Font
gulp.task('fonts', function() {
    return gulp.src(_font.src)
        .pipe(gulp.dest(_font.dest))
        .pipe(browserSync.stream());
});

// Tao server va watching cho JS va CSS
gulp.task(_serverName, ['sass'], function() {
    browserSync.init({
        server: _html.dest
    });

    gulp.watch(_sass.src, ['sass']);
    gulp.watch(_html.src).on("change", browserSync.reload);

});

// move JS chi chay 1 lan
gulp.task('default', _serverMove);